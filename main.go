package main

import (
	"fmt"
	"os"
	"os/exec"
	"io/ioutil"
	"log"
	"github.com/teris-io/cli"
	"path/filepath"
	"strconv"
)

func cliRoot() cli.App {
	return cli.New(ShortDescription()).
		WithCommand(cliGen()).
		WithCommand(cliVersion())
}

func cliVersion() cli.Command {
	return cli.NewCommand("version", "show version").
		WithAction(func(args []string, options map[string]string) int {
			log.Println("0.1.0")
			return 0
		})
}

func ShortDescription() string {
	return "Add sound to your .dem files for team review.\n\n" +
		"    TeamDemo will split you mp3 file for each seconds inside a dedicated folder\n" +
		"    and generate a .vdm file.\n" +
		"    You can then move generated folder, .vdm file and .dem file inside csgo folder.\n"
}

func cliGen() cli.Command {
	return cli.NewCommand("gen", "generate files to put next to demo").
		WithArg(cli.NewArg("demoName", "demo file's name (without the .dem extension)")).
		WithArg(cli.NewArg("mp3Path", "path to mp3 file containing audio communication")).
		WithOption(cli.NewOption("mp3splt", "path to mp3splt folder (default: mp3splt)").WithType(cli.TypeString)).
		WithOption(cli.NewOption("tick-offset", "Wait for a specific tick before start playing sound").WithType(cli.TypeInt)).
		WithAction(func(args []string, options map[string]string) int {
			mp3spltFolder := "./mp3splt"
			if len(options["mp3splt"]) > 0 {
				mp3spltFolder = options["mp3splt"];
			}
			tickOffset := 0
			if len(options["tick-offset"]) > 0 {
				res, err := strconv.Atoi(options["tick-offset"])
				if err != nil {
					log.Fatal(err)
				}
				tickOffset = res
			}
			demoName := args[0]
			mp3Path := args[1]
			return gen(demoName, mp3Path, mp3spltFolder, tickOffset)
		})
}

func gen(demoName string, mp3Path string, mp3spltPath string, tickOffset int) int {
	numberOfParts := genMp3Folder(demoName, mp3Path, mp3spltPath)
	genVdm(demoName, numberOfParts, tickOffset)
	return 0
}

func genMp3Folder(demoName string, mp3Path string, mp3spltFolder string) int {
	currentFolder, err := filepath.Abs(".")
	if err != nil {
		log.Fatal(err)
	}	
	soundFolderPath := currentFolder + "\\" + demoName + ".audio"
	mp3AbsPath, err := filepath.Abs(mp3Path)
	if err != nil {
		log.Fatal(err)
	}
	cmd := exec.Command("./mp3splt.exe", "-d", soundFolderPath, "-o", "audio-@n0", "-t", "00.01", mp3AbsPath)
	cmd.Dir = mp3spltFolder
	fmt.Println("calling " + cmd.String() + " ...")
	output, err := cmd.CombinedOutput()
	fmt.Println(string(output))
	if err != nil {
		log.Fatal(err)
	}
    files,_ := ioutil.ReadDir(soundFolderPath)
    return len(files)
}

func genVdm(demoName string, numberOfParts int, tickOffset int) {
	vdmPath := demoName + ".vdm"
	fmt.Printf("generating %s file ...\n", vdmPath)
	soundFolderPath := demoName + ".audio"
	f, err := os.Create(vdmPath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	/* Example
demoactions
{
	"1"
	{
		factory "PlayCommands"
		name "Play team vocal"
		starttick "1"
		commands "play ../g2-vs-faze-m1-inferno/audio-1.mp3"
	}
}
	*/

	f.WriteString("demoactions\n{\n")
	for i := 1; i < numberOfParts; i++ {
		f.WriteString(fmt.Sprintf("    \"%d\"\n", i))
		f.WriteString(fmt.Sprintf("    {\n"))
		f.WriteString(fmt.Sprintf("        factory \"PlayCommands\"\n"))
		f.WriteString(fmt.Sprintf("        name \"Play audio part %d\"\n", i))
		f.WriteString(fmt.Sprintf("        starttick \"%d\"\n", tickOffset + i * 128 - 127))
		f.WriteString(fmt.Sprintf("        commands \"play ../%s/audio-%d.mp3\"\n", soundFolderPath, i))
		f.WriteString(fmt.Sprintf("    }\n"))
	}
	f.WriteString("}\n")
	fmt.Printf("%s generated\n", vdmPath)
}

func main() {
	log.SetFlags(0)
	app := cliRoot()
	os.Exit(app.Run(os.Args, os.Stdout))
}