# Team Demo!

This command-line tool allow helps users to play some audio while interacting with [valve's demo player](https://developer.valvesoftware.com/wiki/Demo_Recording_Tools).

For instance, this allow teams to review a cs:go demo while listening to team's vocal.

Feel free to open an issue for discussion.

# Command options

```
TeamDemo.exe gen [--mp3splt=string] [--tick-offset=int] <demoName> <mp3Path>

Description:
    generate files to put next to demo

Arguments:
    demoName            demo file's name (without the .dem extension)
    mp3Path             path to mp3 file containing audio communication

Options:
        --mp3splt       path to mp3splt folder (default: mp3splt)
        --tick-offset   Wait for a specific tick before start playing sound
```

# Installation and Usage

## Pre-requists

- Windows machine (the project is not adapted yet for Linux, sorry)
- Some software like [Audacity](audacityteam.org/) to edit mp3
- Know how to run commands in command-line prompt
- Identify your cs:go folder (usually `somewhere\SteamLibrary\steamapps\common\Counter-Strike Global Offensive\csgo`)
- Know how to run demo (using `demoui` command)
- Get your demo file
- Get your audio file (team chats can be recorded with [craig.chat](https://craig.chat/), [Mumble](https://www.mumble.info/), [teamspeak](https://teamspeak.com/en/), etc)

## Prepare your audio file

Get your audio and make sure it start at the very beginning of the demo file.

This can be tricky to have so you can also:
1. Edit the audio file and make the mp3 start at a well known time (example: at the first second of the first round)
2. Open the demo file ingame and know at which tick is the well known time (ex: 2533)
3. use `--tick-offset` option in the next section

## Convert audio with TeamDemo

Here, we are using an example where the demo file is named `mydemo.dem`.

1. Get pre-packaged installation zip file in [releases](https://gitlab.com/mojo42/teamdemo/-/tags)
2. Extract zip file (you can do it directly in `csgo` folder )
3. Open your console, go to extracted folder and run:
```
TeamDemo.exe gen mydemo myRecord.mp3
```
4. (optional) add `--tick-offset` if you need to skip some time
```
TeamDemo.exe gen --tick-offset 2533 mydemo myRecord.mp3
```
5. You should now have generated `mydemo.vdm` file and `mydemo.audio` folder.
6. Place both file/folder next to your .dem file (inside csgo folder, this is need to be here)
7. Open demo file, sound should start and you should be able to navigate in .dem file

# How does it work?

`.vdm` file is a script which starts when running a `.dem` file. This script will play generated audio files at the corresponding tick.

# Known issues / limitations

This project still in early alpha and has a number of issues:
- Sound has a small gap every second (audio parts are 1 second long, could be an option).
- Moving in time mostly work but demo viewer sometime refuse to properly run any `play` command. By waiting few seconds, sound should come back.
- Project is not yet ready to use out of the box for linux users.

# Developping Team Demo

- Building: install golang and just run `go build`.
- You will need to build [mp3splt](http://mp3splt.sourceforge.net/mp3splt_page/home.php) folder next to TeamDemo.exe or use the one in releases.

# Licenses

- TeamDemo is licensed under BSD-3-Clause, see LICENSE.txt file for more details
- mp3splt is licensed under GNU General Public License v2.0, see mp3splt folder for more details